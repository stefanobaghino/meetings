package me.baghino.meetings

import java.lang.Math._
import java.lang.System._

import org.apache.flink.api.java.utils.ParameterTool
import org.slf4j.Logger

/**
  * The set of parameters required for the job to run
  * @param inputPath The location of the file that will be read
  * @param parallelism The parallelism factor of the job (i.e. number of task slots)
  * @param range The spatial range within which a meeting must occur
  * @param timeWindow The temporal window within which a meeting must occur
  * @param query What to look for
  */
case class Parameters(inputPath: String, parallelism: Int, range: Double, timeWindow: Int, query: Query) {
  require(parallelism > 0, "parallelism should be positive")
  require(range > 0, "meeting range should be positive")
  require(timeWindow > 0, "time window should be positive")
  require(timeWindow / 1000 + range <= 30, "the sum of range and time window should be less than or equal to 30")
}

/**
  * Utility module for the job (just to reduce the clutter in the Main class)
  */
object MainUtils {

  /**
    * Reads the parameters for the job, fail spectacularly if anything goes wrong to terminate early
    * @param args The program arguments
    * @param onError The callback to be invoked when an error occurs
    * @return The parsed parameters
    */
  def getParameters(args: Array[String], onError: Exception => Unit): Parameters = {
    try {
      val parameters = ParameterTool.fromArgs(args)
      val input = parameters.getRequired("input")
      val parallelism = parameters.getInt("parallelism", sys.runtime.availableProcessors)
      val range = parameters.getDouble("range")
      val timeWindow = parameters.getInt("timeWindow") * 1000
      val query = parameters.getRequired("query")
      Parameters(input, parallelism, range, timeWindow, Query.parse(query))
    } catch {
      case e: Exception =>
        onError(e)
        throw e
    }
  }

  /**
    * Run a process and perform some kind of reporting based on the timing
    * @param reportFor The reporting function
    * @param timedProcess The process to be ran and timed
    * @tparam A The output type of the report (will probably by Unit)
    * @return The output of the report
    */
  def reportTiming[A](reportFor: Long => A)(timedProcess: => Any): A = {
    val start = currentTimeMillis
    timedProcess
    reportFor(currentTimeMillis - start)
  }

  /**
    * Outputs a short report on time and memory usage on a given logger
    * @param logger The logger used to output the report
    * @param duration The job duration
    */
  def indexingReport(logger: Logger)(duration: Long): Unit = {
    logger.info(s"Memory (max)   : ${humanReadableByteCount(sys.runtime.maxMemory)}")
    logger.info(s"Memory (total) : ${humanReadableByteCount(sys.runtime.totalMemory)}")
    logger.info(s"Memory (free)  : ${humanReadableByteCount(sys.runtime.freeMemory)}")
    logger.info(f"Running time   : ${duration / 1000.0}%.1f s")
  }

  /**
    * Take a number (of bytes) and format them in a
    * Shamelessly copied from StackOverflow
    * http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
    * @param bytes The number of bytes
    * @return A formatted string expressing the number of bytes in a readable form
    */
  private def humanReadableByteCount(bytes: Long): String = {
    if (bytes < 1000) {
      s"$bytes B"
    } else {
      val exp = (log(bytes) / log(1000)).toInt
      val pre = "KMGTPE".charAt(exp - 1)
      val num = bytes / pow(1000, exp)
      f"$num%.1f $pre%sB"
    }
  }

}
