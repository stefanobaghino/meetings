package me.baghino.meetings

/**
  * A confidence function maps two positions to a value between 0.0 and 1.0 which signifies
  * how much the function is sure about the positions taking part in a meeting
  */
sealed trait Confidence extends ((Position, Position) => Double)

object Confidence {

  /**
    * Normalize the spatial distance between two positions, given a certain range
    *
    * @param p1 A position
    * @param p2 Another position
    * @param range The normalization range
    * @return
    */
  private def normalizedSpatialDistance(p1: Position, p2: Position, range: Double): Double =
    p1.spatialDistance(p2) / range

  /**
    * Normalize the temporal distance between two positions, given a certain window
    *
    * @param p1 A position
    * @param p2 Another position
    * @param window The normalization window
    * @return
    */
  private def normalizedTemporalDistance(p1: Position, p2: Position, window: Int): Double =
    p1.temporalDistance(p2) / window.toDouble
  /**
    * A simple generator whose confidence function returns 1.0 if a position
    * is within a given range from the other and 0.0 otherwise
    *
    * @param range
    * @return
    */
  case class PseudoPredicate(range: Double) extends Confidence {
    override def apply(p1: Position, p2: Position): Double =
      if (p1.spatialDistance(p2) < range) 1.0 else 0.0
  }

  /**
    * A generator that creates a function that maps linearly the normalized spatial
    * and temporal distances to a confidence value between 0.0 and 1.0
    *
    * @param range
    * @param timeWindow
    * @return
    */
  case class Linear(range: Double, timeWindow: Int) extends Confidence {
    override def apply(p1: Position, p2: Position): Double =
      1.0 - (normalizedSpatialDistance(p1, p2, range) / 2 + normalizedTemporalDistance(p1, p2, timeWindow) / 2)
  }

  /**
    * A generator that creates a function that maps "exponentially" the normalized spatial
    * and temporal distances to a confidence value between 0.0 and 1.0
    *
    * @param range
    * @param timeWindow
    * @return
    */
  case class Exponential(range: Double, timeWindow: Int) extends Confidence {
    override def apply(p1: Position, p2: Position): Double =
      1.0 - (normalizedSpatialDistance(p1, p2, range) * normalizedTemporalDistance(p1, p2, timeWindow))
  }

}
