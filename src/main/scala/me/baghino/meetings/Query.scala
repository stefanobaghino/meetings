package me.baghino.meetings

import scala.util.{Failure, Success, Try}

object Query {

  private val queryRegex = "(\\w+),(\\w+)(,((0|1)?(\\.(\\d+))?))?".r

  /**
    * Parse a query and extract the relevant information
    * Fails spectacularly to terminate early as this parsing is part of the startup phase
    * @param query A query string to be parsed
    * @return A query object containing the constraint as they are expressed by the user
    */
  def parse(query: String): Query =
    query match {
      case queryRegex(id1, id2, null, _, _, _, _) => Query(id1, id2)
      case queryRegex(id1, id2, _, c, _, _, _) =>
        Try(c.toDouble) match {
          case Success(c) if c >= 0.0 && c <= 1.0 => Query(id1, id2, c)
          case Success(x) => throw OutOfRangeException(x)
          case Failure(e) => throw NumberFormatException(c, e)
        }
      case _ => throw SyntaxErrorException(query)
    }
}

sealed abstract class QueryParseException(message: String, cause: Throwable = null) extends RuntimeException(message, cause)
case class OutOfRangeException(x: Double) extends QueryParseException(s"confidence threshold value $x is out of range (0.0, 1.0)")
case class NumberFormatException(s: String, e: Throwable) extends QueryParseException(s"confidence threshold string '$s' is not a number", e)
case class SyntaxErrorException(q: String) extends QueryParseException(s"syntax error in query '$q'")

case class Query(uid1: String, uid2: String, confidenceThreshold: Double = 0.0)