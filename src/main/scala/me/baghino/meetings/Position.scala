package me.baghino.meetings

import java.lang.Math.sqrt
import java.lang.Math.pow
import java.lang.Math.abs
import java.time.ZonedDateTime

import scala.util.Try

object Position {

  /**
    * Parses a line into a position
    * @param row A string
    * @return A position if the string is well formed, fails otherwise
    */
  def parse(row: String): Try[Position] =
    Try {
      val Array(timestamp, x, y, floor, uid) = row.split(",")
      Position(
        timestamp = ZonedDateTime.parse(timestamp).toInstant.toEpochMilli,
        x = x.toDouble,
        y = y.toDouble,
        floor = floor.toInt,
        uid = uid)
    }

}

/**
  * Maps directly to the input as it is expected to come
  * @param timestamp The timestamp expressed as a UNIX epoch
  * @param x The x coordinate of the position
  * @param y The y coordinate of the position
  * @param floor The floor at which the reading happened
  * @param uid The identifier of the user whose position has been detected
  */
case class Position(timestamp: Long, x: Double, y: Double, floor: Int, uid: String) {

  /**
    * Euclidean distance from another position
    * @param that Another position
    * @return The distance between the two positions
    */
  def spatialDistance(that: Position): Double =
    sqrt(pow(abs(this.x - that.x), 2) + pow(abs(this.y - that.y), 2))

  /**
    * Number of milliseconds between two readings
    * @param that Another position
    * @return The time between two readings, in milliseconds
    */
  def temporalDistance(that: Position): Long =
    abs(this.timestamp - that.timestamp)

}
