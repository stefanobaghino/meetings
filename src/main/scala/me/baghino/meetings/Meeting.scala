package me.baghino.meetings

import java.lang.Math.{max, min}
import java.time.{Instant, ZoneId, ZonedDateTime}

import com.vividsolutions.jts.geom.Envelope

object Meeting {

  /**
    * Convert a UNIX epoch to a UTC date
    * @param millis Number of milliseconds since Jan 1st, 1970
    * @return A UTC date
    */
  private def utcDateFromEpoch(millis: Long): ZonedDateTime =
    ZonedDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.of("UTC"))

  /**
    * Given a collection of collection moments, produce an envelope that
    * expresses the area within which a meeting has happened
    * @param moments A collection of meeting moments
    * @return An envelope containing all the points that pertain to a meeting
    */
  private def area(moments: Iterable[MeetingMoment]): Envelope = {
    val envelope = new Envelope()
    for (moment <- moments) {
      envelope.expandToInclude(moment.x1, moment.y1)
      envelope.expandToInclude(moment.x2, moment.y2)
    }
    envelope
  }

  /**
    * Construct a meeting from a set of unique meeting moments
    * @param uniqueMoments A set of meeting moments that refer to a meeting
    * @return An optional meeting, none if the set of meeting moments is empty
    */
  def fromMoments(uniqueMoments: Set[MeetingMoment]): Option[Meeting] = {
    val moments = uniqueMoments.toVector
    moments.headOption.map {
      case MeetingMoment(_, floor, _, _, _, uid1, _, _, _, uid2) =>
        val from = moments.map(moment => min(moment.timestamp1, moment.timestamp2)).min
        val to = moments.map(moment => max(moment.timestamp1, moment.timestamp2)).max
        val avgConfidence = moments.map(_.confidence).sum / moments.size
        Meeting(avgConfidence, uid1, uid2, utcDateFromEpoch(from), utcDateFromEpoch(to), floor, moments.size, area(moments))
    }
  }

}

/**
  * A data class expressing date regarding a prospective meeting
  * @param confidence How much we are confident that a meeting has actually happened
  * @param uid1 The first involved party
  * @param uid2 The second involved party
  * @param from The moment when the meeting started
  * @param to The moment when the  meeting ended
  * @param floor The floor at which the meeting occurred
  * @param moments The number of meeting moments that are part of the meeting
  * @param area The boundaries within which the meeting occurred
  */
case class Meeting(confidence: Double, uid1: String, uid2: String, from: ZonedDateTime, to: ZonedDateTime, floor: Int, moments: Int, area: Envelope)

/**
  * A data class expressing a moment when two parties may have been involved in a meeting
  * @param confidence How much we are confident that a meeting has actually happened
  * @param floor The floor at which the meeting occurred
  * @param timestamp1 The moment in time at which the first involved party was when the meeting moment occurred
  * @param x1 The x coordinate at which the first involved party was when the meeting moment occurred
  * @param y1 The y coordinate at which the first involved party was when the meeting moment occurred
  * @param uid1 The identifier of the first involved party
  * @param timestamp2 The moment in time at which the second involved party was when the meeting moment occurred
  * @param x2 The x coordinate at which the second involved party was when the meeting moment occurred
  * @param y2 The y coordinate at which the second involved party was when the meeting moment occurred
  * @param uid2 The identifier of the second involved party
  */
case class MeetingMoment(confidence: Double, floor: Int,
                         timestamp1: Long, x1: Double, y1: Double, uid1: String,
                         timestamp2: Long, x2: Double, y2: Double, uid2: String)

/**
  * A function that uses a confidence function and a query to determine whether a meeting happened or not
  * @param confidence The function used to determine the confidence value of a meeting
  * @param query The constraint that have to be satisfied in order to consider a meeting as such
  */
class MeetingChancesFunction(confidence: Confidence, query: Query) extends (Stream[Position] => Stream[MeetingMoment]) with Serializable {

  /**
    * An ordering of two positions in respect to a "pivot" one.
    * Currently unused, may come in handy to construct a sorted set
    * to perform efficient queries over confidence values
    * @param m A "pivot" position on which other two will be compared
    */
  class ConfidenceOrd(m: Position) extends Ordering[Position] {
    override def compare(x: Position, y: Position): Int = {
      (confidence(m, x) - confidence(m, y)) match {
        case n if n < 0 => -1
        case n if n > 0 => +1
        case _ => 0
      }
    }
  }

  /**
    * Take a collection of positions and combine them with
    * each other, making sure not to combine positions twice
    * @param ps A collection of positions
    * @return A collection of meeting moments
    */
  def apply(ps: Stream[Position]): Stream[MeetingMoment] = {
    for {
      ts <- ps.tails.toStream if ts.nonEmpty
      p1 = ts.head
      p2 <- ts.tail if satisfyQuery(p1, p2)
    } yield meeting(p1, p2)
  }

  /**
    * Construct a meeting moment from a pair of positions, making
    * sure that the two positions are sorted by their identifiers
    * so that it will be easier to detect and remove duplicates
    * @param p1 A position
    * @param p2 Another position
    * @return A meeting moment involving the two parties
    */
  private def meeting(p1: Position, p2: Position): MeetingMoment =
    if (p1.uid < p2.uid) {
      MeetingMoment(confidence(p1, p2), p1.floor,
        p1.timestamp, p1.x, p1.y, p1.uid,
        p2.timestamp, p2.x, p2.y, p2.uid)
    } else {
      MeetingMoment(confidence(p1, p2), p1.floor,
        p2.timestamp, p2.x, p2.y, p2.uid,
        p1.timestamp, p1.x, p1.y, p1.uid)
    }

  /**
    * Check that the query is satisfied for a pair of positions.
    * The positions must be from distinct parties, match the
    * identifiers provided and exceed the required confidence value
    * @param p1 A position
    * @param p2 Another position
    * @return True if the positions satisfy the query, false otherwise
    */
  private def satisfyQuery(p1: Position, p2: Position): Boolean =
    p1.uid != p2.uid &&
    (p1.uid == query.uid1 || p1.uid == query.uid2) &&
      (p2.uid == query.uid2 || p2.uid == query.uid1) &&
      confidence(p1, p2) >= query.confidenceThreshold

}
