package me.baghino.meetings

import java.lang.Math.max
import java.time.{ZoneId, ZonedDateTime}

import me.baghino.meetings.MainUtils._
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment, createTypeInformation}
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector
import org.slf4j.LoggerFactory

import scala.util.{Failure, Success}

object Main {

  private val logger = LoggerFactory.getLogger(getClass.getName.init)

  /**
    * First pipeline step: grab the input and parse it
    * @param env Flink's streaming execution environment
    * @param onError Callback to invoke in case of error
    * @param inputPath The location of the input CSV file
    * @return A data stream of parsed data points
    */
  private def parseInput(env: StreamExecutionEnvironment, onError: (String, Throwable) => Unit)(inputPath: String): DataStream[Position] =
    env.readTextFile(inputPath).flatMap {
      (row: String, out: Collector[Position]) =>
        Position.parse(row) match {
          case Success(position) => out.collect(position)
          case Failure(exception) => onError(row, exception)
        }
    }.assignAscendingTimestamps(_.timestamp)

  /**
    * Second step of the pipeline: aggregate by time window, pair matching positions
    * together and check that they satisfy the user requirements
    * @param timeWindow The time window within which a meeting can occur
    * @param find The function used to aggregate and filter prospective meetings
    * @param positions A data stream of parsed data points
    * @return A data stream of meeting moments
    */
  private def findMeetingMoments(timeWindow: Int, find: MeetingChancesFunction)(positions: DataStream[Position]): DataStream[MeetingMoment] =
    positions.keyBy(_.floor).timeWindow(Time.milliseconds(timeWindow), Time.milliseconds(timeWindow / 2)) {
      (_: Int, _: TimeWindow, positions: Iterable[Position], out: Collector[MeetingMoment]) =>
        find(positions.toStream).foreach(out.collect)
    }

  /**
    * Third step of the pipeline: remove duplicates and aggregate meeting moments into less granular meetings
    * @param timeWindow The time window within which a meeting can occur
    * @param meetingMoments A datastream of meeting moments
    * @return A data stream of meetings
    */
  private def aggregateMeetings(timeWindow: Int)(meetingMoments: DataStream[MeetingMoment]): DataStream[Meeting] =
    meetingMoments.assignAscendingTimestamps(m => max(m.timestamp1, m.timestamp2)).
      keyBy(_.floor).timeWindow(Time.milliseconds(timeWindow)) {
      (_: Int, _: TimeWindow, meetingMoments: Iterable[MeetingMoment], out: Collector[Meeting]) =>
        Meeting.fromMoments(meetingMoments.toSet).foreach(out.collect)
    }

  def main(args: Array[String]): Unit = {

    // Parse job parameters
    val params =
      getParameters(args, { error =>
        logger.error("{}", error.getMessage)
        sys.exit(1)
      })

    // Create and instance of the function used to check that positions can be meetings
    val meetingChancesFunction =
      new MeetingChancesFunction(Confidence.Exponential(params.range, params.timeWindow), params.query)

    // Initialize Flink
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    // Ask Flink to have a certain degree of parallelism and to treat
    // data according to a timestamp tied to the event itself (not to processing time)
    env.setParallelism(params.parallelism)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    // Assemble the pipeline
    val meetings =
      parseInput(env, onError = { (row, _) => logger.warn(s"failed to parse row: $row") }) _ andThen
        findMeetingMoments(params.timeWindow, meetingChancesFunction) _ andThen
        aggregateMeetings(params.timeWindow)

    // Tell the pipeline to dump the results on standard output (note: this is lazy, it doesn't execute anything)
    meetings(params.inputPath).print

    reportTiming(indexingReport(logger)) {
      // Please note that all prior methods define the processing pipeline
      // but never execute anything; here the pipeline is finally executed
      env.execute(s"Indexing (${ZonedDateTime.now(ZoneId.of("UTC"))})")
    }

  }

}
