package me.baghino.meetings

import org.scalatest._

class QuerySpec extends FlatSpec with Matchers {

  "The string 'id1,id2'" should "be parsed correctly" in {
    Query.parse("id1,id2") shouldBe Query("id1", "id2")
  }

  "The string 'id1,id2,0.0'" should "be parsed correctly" in {
    Query.parse("id1,id2,0.0") shouldBe Query("id1", "id2", 0.0)
  }

  "The string 'id1,id2,1.0'" should "be parsed correctly" in {
    Query.parse("id1,id2,1.0") shouldBe Query("id1", "id2", 1.0)
  }

  "The string 'id1,id2,0.42'" should "be parsed correctly" in {
    Query.parse("id1,id2,0.42") shouldBe Query("id1", "id2", 0.42)
  }

  "The string 'id1,id2,.42'" should "be parsed correctly" in {
    Query.parse("id1,id2,.42") shouldBe Query("id1", "id2", 0.42)
  }

  "The string 'id1,id2,0'" should "be parsed correctly" in {
    Query.parse("id1,id2,0") shouldBe Query("id1", "id2", 0.0)
  }

  "The string 'id1,id2,1'" should "be parsed correctly" in {
    Query.parse("id1,id2,1") shouldBe Query("id1", "id2", 1.0)
  }

  "The string 'id1,id2,1.42'" should "not be parsed correctly" in {
    an[OutOfRangeException] shouldBe thrownBy(Query.parse("id1,id2,1.42"))
  }

  "The string 'id1,id2,'" should "not be parsed correctly" in {
    a[NumberFormatException] shouldBe thrownBy(Query.parse("id1,id2,"))
  }

  "The string 'interactive'" should "not be parsed correctly" in {
    a[SyntaxErrorException] shouldBe thrownBy(Query.parse("interactive"))
  }

}
