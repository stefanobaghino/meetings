resolvers in ThisBuild ++= Seq(Resolver.mavenLocal)

name := "meetings"

version := "0.1-SNAPSHOT"

organization := "me.baghino"

scalaVersion in ThisBuild := "2.11.7"

val flinkVersion = "1.0.2"

val flinkDependencies = Seq(
  "org.apache.flink" %% "flink-scala" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-streaming-scala" % flinkVersion % "provided")

val spatialDependencies = Seq(
  "com.vividsolutions" % "jts" % "1.13"
)

val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "2.2.6" % "test"
)

lazy val root = (project in file(".")).
  settings(
    libraryDependencies ++= flinkDependencies ++ spatialDependencies ++ testDependencies
  )

mainClass in assembly := Some("me.baghino.meetings.Main")

fork in run := true

// make run command include the provided dependencies
run in Compile <<= Defaults.runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run))

// exclude Scala library from assembly
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
